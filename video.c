#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {

	// don't name file like temp.avi
	if ( argc != 3 ) {
		printf( "Usage: %s /path/to/file filename.avi\n", argv[0] );
		return -1;
	}

	// try to make a directory
	char new_dir_script[500];
	sprintf( new_dir_script, "mkdir %s", argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", new_dir_script );
	puts( "==========" );
	system( new_dir_script );

	// enable LED
	system( "echo \"238\" | sudo tee /sys/class/gpio/export" );
	system( "echo \"out\" | sudo tee /sys/class/gpio/gpio238/direction" );
	system( "echo \"1\" | sudo tee /sys/class/gpio/gpio238/value" );

	// run GStreamer script
	char gst_script[500];
	sprintf( gst_script, "gst-launch-1.0 -e uvch264src iframe-period=100 auto-start=true device=/dev/video0 fixed-framerate=true initial-bitrate=10000000 average-bitrate=10000000 peak-bitrate=10000000 ! queue ! image/jpeg, width=1920, height=1080, framerate=30/1 ! videorate ! image/jpeg, width=1920, height=1080, framerate=30/1 ! mux. alsasrc provide-clock=false do-timestamp=true  device=\"plughw:CARD=Device\" ! queue ! audio/x-raw, rate=48000 ! volume volume=3.0 ! mux. avimux name=mux ! filesink sync=true location=%s/temp.avi", argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", gst_script );
	puts( "==========" );
	system( gst_script );

	//disable LED
	system( "echo \"0\" | sudo tee /sys/class/gpio/gpio238/value" );
	
	// run ffmpeg script 1 
	char ffmpeg_script1[500];
	sprintf( ffmpeg_script1, "ffmpeg -i %s/temp.avi -acodec copy -vcodec copy -y -ss 00:00:00.4 %s/_temp.avi", argv[1], argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", ffmpeg_script1 );
	puts( "==========" );
	system( ffmpeg_script1 );

	// run ffmpeg script 2 
	char ffmpeg_script2[500];
	sprintf( ffmpeg_script2, "ffmpeg -i %s/_temp.avi -acodec copy -vcodec copy -y -ss -00:00:01 %s/%s", argv[1], argv[1], argv[2] );
	puts( "==========" );
	printf( "Running: %s\n", ffmpeg_script2 );
	puts( "==========" );
	system( ffmpeg_script2 );

	// clean up all temp files on finish
	char cleanup_script[500];
	sprintf( cleanup_script, "rm %s/temp.avi", argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", cleanup_script );
	puts( "==========" );
	system( cleanup_script );

	return 0;
}
