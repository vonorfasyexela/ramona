#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {

	// new usage
	// picture directory filename.jpg
	// don't name result file as temp.jpg

	if ( argc != 3 ) {
		printf( "Usage: %s /path/to/file filename.jpg\n", argv[0] );
		return -1;
	}

	// making new directory
	char new_dir_script[500];
	sprintf( new_dir_script, "mkdir %s", argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", new_dir_script );
	puts( "==========" );
	system( new_dir_script );

	// enable LED
	system( "echo \"238\" | sudo tee /sys/class/gpio/export" );
	system( "echo \"out\" | sudo tee /sys/class/gpio/gpio238/direction" );
	system( "echo \"1\" | sudo tee /sys/class/gpio/gpio238/value" );

	// run GStreamer for capturing in /path/to/test.avi
	char gst_script[500];
	sprintf( gst_script, "gst-launch-1.0 uvch264src num-buffers=60 auto-start=true device=/dev/video0 fixed-framerate=true initial-bitrate=10000000 average-bitrate=10000000 peak-bitrate=10000000 ! queue ! image/jpeg, width=1920, height=1080, framerate=30/1 ! avimux ! filesink sync=true location=%s/temp.avi", argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", gst_script );
	puts( "==========" );
	system( gst_script );

	 // disable LED
        system( "echo \"0\" | sudo tee /sys/class/gpio/gpio238/value" );

	// little break
	// change amount here if needed
	system( "sleep 0.2" );

	// flashing
	system( "echo \"238\" | sudo tee /sys/class/gpio/export" );
        system( "echo \"out\" | sudo tee /sys/class/gpio/gpio238/direction" );
        system( "echo \"1\" | sudo tee /sys/class/gpio/gpio238/value" );
	system( "sleep 0.1" );
	system( "echo \"0\" | sudo tee /sys/class/gpio/gpio238/value" );

	// run ffmpeg for making picture from /path/to/test.avi
	char ffmpeg_script[500];
	sprintf( ffmpeg_script, "ffmpeg -i %s/temp.avi -y -ss 00:00:01.5 -vframes 1 -f mjpeg %s/temp.jpg", argv[1], argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", ffmpeg_script );
	puts( "==========" );
	system( ffmpeg_script );

	// converting jpeg with frames to bmp 
	// and then from bmp to jpeg 1-frame
	char for_convert_one[500];
	char for_convert_two[500];
	sprintf( for_convert_one, "convert \"%s/temp.jpg[0]\" %s/temp.bmp", argv[1], argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", for_convert_one );
	puts( "==========" );
	system( for_convert_one );

	sprintf( for_convert_two, "convert -quality 100 %s/temp.bmp %s/%s", argv[1], argv[1], argv[2] );
	puts( "==========" );
	printf( "Running: %s\n", for_convert_two );
	puts( "==========" );
	system( for_convert_two );

	// clean up all temp files on finish
	char cleanup_script[500];
	sprintf( cleanup_script, "rm %s/temp.avi %s/temp.jpg %s/temp.bmp", argv[1], argv[1], argv[1] );
	puts( "==========" );
	printf( "Running: %s\n", cleanup_script );
	puts( "==========" );
	system( cleanup_script );

	return 0;
}
